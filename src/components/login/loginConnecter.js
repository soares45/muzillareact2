import React, {Component} from 'react';

class LoginConnecter extends Component {

  render() {
    return (
      <div className="row">
        <div className="col-xs-7 p-t-5">
          <input type="checkbox" name="rememberme" id="rememberme" className="filled-in chk-col-pink" />
          <label htmlFor="rememberme">Rester connecté</label>
        </div>
        <div className="col-xs-5">
          <button className="btn btn-block bg-pink waves-effect" type="submit">Se connecter</button>
        </div>
      </div>
    );
  }
}

export default LoginConnecter;
