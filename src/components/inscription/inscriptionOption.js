import React, {Component} from 'react';

class InscriptionOption extends Component {

  render() {
    return (
      <div>
        <div className="m-t-25 m-b--5 align-center">
          <a href="Connection.html">Vous avez déjà un compte?</a>
        </div>
        <div className="m-t-25 m-b--5 align-center">
          <a href="OubliMotPasse.html">Jai oublié mon mot de passe?</a>
        </div>
      </div>
    );
  }
}

export default InscriptionOption;
