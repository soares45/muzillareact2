import React, {Component} from 'react';

class EntrepriseHeader extends Component {

  render() {
    return (
      <div className="header">
        <h2>Entreprise #3</h2>
        <ul className="header-dropdown m-r--5">
          <li className="dropdown">
            <a href="javascript:void(0);" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i className="material-icons">more_vert</i>
            </a>
            <ul className="dropdown-menu pull-right">
              <li><a href="pages\Entreprise.html">Voir profile</a></li>
              <li><a href="javascript:void(0);">Ajouter aux favorites</a></li>
              <li><a href="javascript:void(0);">Ne plus montrer</a></li>
            </ul>
          </li>
        </ul>
      </div>
    );
  }
}

export default EntrepriseHeader;
