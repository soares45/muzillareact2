import React, {Component} from 'react';
import NavigationPrincipale from './menu/navigationPrincipale';
import Bibiotheque from './menu/bibiotheque';

class Footer extends Component {

  render() {
    return (
      <div className="menu">
        <ul className="list">
          <NavigationPrincipale />
          <Bibiotheque />
        </ul>
      </div>
    );
  }
}

export default Footer;
