import axios from 'axios';

export const FETCH_LOGIN = 'fetch_login';
export const FETCH_LOGIN_MUSIQUE = 'fetch_login_musique';
export const FETCH_LOGIN_PLAYLIST = 'fetch_login_playlist';
export const AJOUT_MUSIQUE_LECTEUR = 'ajout_musique_lecteur';

const ROOT_URL = 'http://localhost:8094';

export function fetchLogin(username, password){
  const request = axios.get(`${ROOT_URL}/client/login`, {
    username:{username},
    password: {password}
  });

  return {
    type: FETCH_LOGIN,
    payload: request
  }
}

export function fetchLoginMusique(clientId){
  const request = axios.get(`${ROOT_URL}/musique/findByClient/${clientId}`);

  return {
    type: FETCH_LOGIN_MUSIQUE,
    payload: request
  }
}

export function fetchLoginPlaylist(clientId){
  const request = axios.get(`${ROOT_URL}/playlist/findByClient/${clientId}`);

  return {
    type: FETCH_LOGIN_PLAYLIST,
    payload: request
  }
}

export function ajoutMusiqueLecteur(musique){
  return {
    type: AJOUT_MUSIQUE_LECTEUR,
    payload: musique
  }
}
