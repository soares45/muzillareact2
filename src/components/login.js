import React, {Component} from 'react';
import LoginCard from './login/loginCard';
import LoginLogo from './login/loginLogo';

class Login extends Component {

  render() {
    return (
      <div className="login-page">
        <div className="login-box">
          <LoginLogo />
          <LoginCard />
        </div>
      </div>
    );
  }
}

export default Login;
