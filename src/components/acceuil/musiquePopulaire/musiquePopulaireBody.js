import React, {Component} from 'react';
import MusiqueList from './musiqueList';
import MusiqueListItem from './musiqueListItem';

class MusiquePopulaireBody extends Component {

  render() {
    return (
      <div className="body">
        {/* <MusiqueList /> */}
        <div className="row">
          <MusiqueListItem />
          <MusiqueListItem />
          <MusiqueListItem />
          <MusiqueListItem />
        </div>
      </div>
    );
  }
}

export default MusiquePopulaireBody;
