import React, {Component} from 'react';

class Footer extends Component {

  render() {
    return (
      <div className="legal">
        <div className="copyright">
          © 2017 <a href="javascript:void(0);">Muzilla</a>.
        </div>
        <div className="version">
          <b>Version: </b> 1.0.4
        </div>
      </div>
    );
  }
}

export default Footer;
