import React, {Component} from 'react';

class PresentationNos extends Component {

  render() {
    return (
      <div id="gtco-features-2">
        <div className="gtco-container">
          <div className="row">
            <div className="col-md-8 col-md-offset-2 text-center gtco-heading">
              <h2>Pourquoi nous choisir</h2>
              <p>C`est facile.</p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="feature-left animate-box" data-animate-effect="fadeInLeft">
                <span className="icon">
                  <i className="icon-search"/>
                </span>
                <div className="feature-copy">
                  <h3>La recherche</h3>
                  <p>Sachez ce que vous voulez écouter? Recherchez et appuyez sur play.</p>
                </div>
              </div>
              <div className="feature-left animate-box" data-animate-effect="fadeInLeft">
                <span className="icon">
                  <i className="icon-browser"/>
                </span>
                <div className="feature-copy">
                  <h3>Feuilleter</h3>
                  <p>Découvrez les derniers graphiques, les nouvelles versions et les bonnes playlists pour le moment.</p>
                </div>
              </div>
              <div className="feature-left animate-box" data-animate-effect="fadeInLeft">
                <span className="icon">
                  <i className="icon-aircraft-take-off"/>
                </span>

                <div className="feature-copy">
                  <h3>Découvrir</h3>
                  <p>Profitez de la nouvelle musique tous les lundis avec votre propre liste de lecture personnelle. Ou asseyez-vous et profitez de la radio.</p>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="gtco-video gtco-bg" style={{
                backgroundImage: 'url(images/page-principale/img_1.jpg)'
              }}>
                <img src='images/page-principale/soundEquipemment.PNG'/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PresentationNos;
