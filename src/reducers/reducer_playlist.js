import _ from 'lodash';
import { FETCH_LOGIN_MUSIQUE } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case FETCH_LOGIN_MUSIQUE:
      return action.payload.data;
      break;
    default:
      return state;
  }
}
