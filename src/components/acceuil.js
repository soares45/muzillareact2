import React, {Component} from 'react';
import Header from './header';
import EntrepriseList from './acceuil/entrepriseList';
import EntrepriseListItem from './acceuil/entrepriseListItem';
import MusiquePopulaire from './acceuil/musiquePopulaire';

class Acceuil extends Component {

  render() {
    return (
      <div>
      <Header />
      <section className="content">
        <div className="container-fluid">
          {/* <EntrepriseList /> */}
          <div className="row clearfix">
            <EntrepriseListItem />
            <EntrepriseListItem />
            <EntrepriseListItem />
          </div>
          {/* Custom Content */}
          <MusiquePopulaire />
        </div>
      </section>
      </div>
    );
  }
}

export default Acceuil;
