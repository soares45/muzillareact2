import React, {Component} from 'react';

class PresentationDescription extends Component {

  render() {
    return (
      <div id="gtco-features">
        <div className="gtco-container">
          <div className="row">
            <div className="col-md-4 col-sm-4">
              <div className="feature-center animate-box" data-animate-effect="fadeIn">
                <span className="icon">
                  <i className="icon-music"/>
                </span>
                <h3>Musique</h3>
                <p>Il y a des millions de chansons sur Muzilla. Jouez à vos favoris, découvrez de nouveaux morceaux et créez la collection parfaite.</p>
              </div>
            </div>
            <div className="col-md-4 col-sm-4">
              <div className="feature-center animate-box" data-animate-effect="fadeIn">
                <span className="icon">
                  <i className="icon-air-play"/>
                </span>
                <h3>Listes de lecture</h3>
                <p>Vous trouverez des listes de lecture prêtes à l`emploi selon votre humeur, rassemblées par des fans de musique et des experts.</p>
              </div>
            </div>
            <div className="col-md-4 col-sm-4">
              <div className="feature-center animate-box" data-animate-effect="fadeIn">
                <span className="icon">
                  <i className="icon-new"/>
                </span>
                <h3>Nouvelles versions</h3>
                <p>Écoutez les derniers singles et albums de cette semaine et découvrez ce qu`il y a de mieux dans le Top 50.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PresentationDescription;
