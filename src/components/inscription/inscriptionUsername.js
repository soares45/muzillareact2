import React, {Component} from 'react';

class InscriptionLogo extends Component {

  render() {
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">person</i>
        </span>
        <div className="form-line">
          <input type="text" className="form-control" name="namesurname" placeholder="Nom d'utilisateur" required autofocus />
        </div>
      </div>
    );
  }
}

export default InscriptionLogo;
