import React, {Component} from 'react';

class LoginLogo extends Component {

  render() {
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">person</i>
        </span>
        <div className="form-line">
          <input type="text" className="form-control" name="username" placeholder="Nom d'utilisateur" required autofocus />
        </div>
      </div>
    );
  }
}

export default LoginLogo;
