import React, {Component} from 'react';

class EntrepriseControleurLeft extends Component {

  render() {
    return (
      <a className="left carousel-control" href="#carousel-example-generic_3" role="button" data-slide="prev">
        <span className="glyphicon glyphicon-chevron-left" aria-hidden="true" />
        <span className="sr-only">Previous</span>
      </a>
    );
  }
}

export default EntrepriseControleurLeft;
