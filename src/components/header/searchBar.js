import React, {Component} from 'react';

class SearchBar extends Component {

  render() {
    return (
      <div className="search-bar">
        <div className="search-icon">
          <i className="material-icons">search</i>
        </div>
        <input type="text" id="rechercheInput" placeholder="commencer à écrire..." />
        <div className="close-search">
          <i className="material-icons">close</i>
        </div>
      </div>
    );
  }
}

export default SearchBar;
