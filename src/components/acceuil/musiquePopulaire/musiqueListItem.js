import React, {Component} from 'react';

class MusiqueListItem extends Component {

  render() {
    return (
      <div className="col-sm-6 col-md-3">
        <div className="thumbnail">
          <img src="http://placehold.it/500x300" />
          <div className="caption">
            <h3>Musique #1</h3>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy
              text ever since the 1500s
            </p>
            <p>
              <a href="pages/Musique.html" className="btn btn-primary waves-effect" role="button">PLAY</a>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default MusiqueListItem;
