import React, {Component} from 'react';

class InscriptionEmail extends Component {

  render() {
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">email</i>
        </span>
        <div className="form-line">
          <input type="email" className="form-control" name="email" placeholder="Votre adresse Email" required />
        </div>
      </div>
    );
  }
}

export default InscriptionEmail;
