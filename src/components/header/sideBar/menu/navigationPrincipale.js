import React, {Component} from 'react';

class NavigationPrincipale extends Component {

  render() {
    return (
      <div>
      <li className="header">NAVIGATION PRINCIPALE</li>
      <li>
        <a href="index.html">
          <i className="material-icons">home</i>
          <span>Accueil</span>
        </a>
      </li>
      <li>
        <a href="pages/Profile.html">
          <i className="material-icons">person</i>
          <span>Profile</span>
        </a>
      </li>
      <li className="active">
        <a href="pages/Playlist.html">
          <i className="material-icons">history</i>
          <span>Historique</span>
        </a>
      </li>
      <li>
        <a href="pages/Playlist.html">
          <i className="material-icons">favorite</i>
          <span>favorite</span>
        </a>
      </li>
      </div>
    );
  }
}

export default NavigationPrincipale;
