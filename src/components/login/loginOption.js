import React, {Component} from 'react';

class LoginOption extends Component {

  render() {
    return (
      <div className="row m-t-15 m-b--20">
        <div className="col-xs-4">
          <a href="Inscription.html">Sinscrire!</a>
        </div>
        <div className="col-xs-8 align-right">
          <a href="OubliMotPasse.html">Jai oublié mon mot de passe?</a>
        </div>
      </div>
    );
  }
}

export default LoginOption;
