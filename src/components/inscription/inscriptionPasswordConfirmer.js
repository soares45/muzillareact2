import React, {Component} from 'react';

class InscriptionPasswordConfirmer extends Component {

  render() {
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">lock</i>
        </span>
        <div className="form-line">
          <input type="password" className="form-control" name="confirm" minLength={6} placeholder="Confirmer le mot de passe" required />
        </div>
      </div>
    );
  }
}

export default InscriptionPasswordConfirmer;
