import React, {Component} from 'react';

class CarouselListItem extends Component {

  render() {
    return (
      <div className="item">
        <img src="images/image-gallery/10.jpg" />
        <div className="carousel-caption">
          <h3>Album #1</h3>
          <p>Je suis une prensentation de lalbum.</p>
        </div>
      </div>
    );
  }
}

export default CarouselListItem;
