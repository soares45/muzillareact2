import React, {Component} from 'react';
import LoginUsername from './loginUsername';
import LoginPassword from './loginPassword';
import LoginConnecter from './loginConnecter';
import LoginOption from './loginOption';

class LoginCard extends Component {

  render() {
    return (
      <div className="card">
        <div className="body">
          <form id="sign_in" method="POST" action="../index.html">
            <div className="msg">Connexion à votre compte</div>
            <LoginUsername />
            <LoginPassword />
            <LoginConnecter />
            <LoginOption />
          </form>
        </div>
      </div>
    );
  }
}

export default LoginCard;
