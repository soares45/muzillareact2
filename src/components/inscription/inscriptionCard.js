import React, {Component} from 'react';
import InscriptionUsername from './inscriptionUsername';
import InscriptionEmail from './inscriptionEmail';
import InscriptionPassword from './inscriptionPassword';
import InscriptionPasswordConfirmer from './inscriptionPasswordConfirmer';
import InscriptionValide from './inscriptionValide';
import InscriptionOption from './inscriptionOption';

class InscriptionCard extends Component {

  render() {
    return (
      <div className="card">
        <div className="body">
          <form id="sign_up" method="POST" action="../index.html">
            <div className="msg">Enregistrez un nouveau compte</div>
            <InscriptionUsername />
            <InscriptionEmail />
            <InscriptionPassword />
            <InscriptionPasswordConfirmer />
            <InscriptionValide />
            <button className="btn btn-block btn-lg bg-pink waves-effect" type="submit">Sinscrire!</button>
            <InscriptionOption />
          </form>
        </div>
      </div>
    );
  }
}

export default InscriptionCard;
