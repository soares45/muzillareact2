import React, {Component} from 'react';

class InscriptionValide extends Component {

  render() {
    return (
      <div className="form-group">
        <input type="checkbox" name="terms" id="terms" className="filled-in chk-col-pink" />
        <label htmlFor="terms">J'ai lu et j'accepte les <a href="javascript:void(0);">termes dutilisation</a>.</label>
      </div>
    );
  }
}

export default InscriptionValide;
