import React, {Component} from 'react';
import MusiquePopulaireHeader from './musiquePopulaire/musiquePopulaireHeader';
import MusiquePopulaireBody from './musiquePopulaire/musiquePopulaireBody';

class MusiquePopulaire extends Component {

  render() {
    return (
      <div className="row clearfix">
      <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div className="card">
          <MusiquePopulaireHeader />
          <MusiquePopulaireBody />
        </div>
      </div>
      </div>
    );
  }
}

export default MusiquePopulaire;
