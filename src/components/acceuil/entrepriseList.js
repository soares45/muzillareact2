import React from 'react';
import EntrepriseListItem from './entrepriseListItem';

const EntrepriseList = ({videos}) => {
  const EntrepriseListItems = videos.map((videos) => {
    return (
      <EntrepriseListItem />
    );
  });
  return (
    <div className="row clearfix">
      {EntrepriseListItems}
    </div>
  );
};

export default EntrepriseList;
