import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import UtilisateurSession from './reducer_session';
import PlaylistReducer from './reducer_playlist';
import MusiqueReducer from './reducer_musique';
import LecteurMusiqueReducer from './reducer_lecteur';

const rootReducer = combineReducers({
  user: UtilisateurSession,
  playlist: PlaylistReducer,
  musique: MusiqueReducer,
  LecteurMusique: LecteurMusiqueReducer,
  form: formReducer
});

export default rootReducer;
