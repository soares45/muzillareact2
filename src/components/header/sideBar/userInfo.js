import React, {Component} from 'react';

class UserInfo extends Component {

  render() {
    return (
      <div className="user-info">
        <div className="image">
          <img src="images/user.png" width={48} height={48} alt="User" />
        </div>
        <div className="info-container">
          <div className="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Alexandre Soares</div>
          <div className="email">soares.alex45@gmail.com</div>
          <div className="btn-group user-helper-dropdown">
            <i className="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
            <ul className="dropdown-menu pull-right">
              <li><a href="pages/Profile.html"><i className="material-icons">person</i>Profile</a></li>
              <li role="seperator" className="divider" />
              <li><a href="pages/Connection.html"><i className="material-icons">input</i>Déconnexion</a></li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default UserInfo;
