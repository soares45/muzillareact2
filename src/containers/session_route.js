import React, { Component } from 'react';
import { connect } from 'react-redux';
import Acceuil from '../components/acceuil';
import Login from '../components/login';
import Inscription from '../components/inscription';
import Presentation from '../components/presentation';

class SessionRoute extends Component {

  render() {
    const { UtilisateurSession } = this.props;
    if (UtilisateurSession && UtilisateurSession.id) {
      return (
        <div>
          <Route path="/accueil" component={Acceuil} />
          <Route path="/inscription" component={Inscription} />
          <Route path="/connection" component={Login} />
          <Route path="/" component={Presentation} />
        </div>
      );
    } else {
      return (<Presentation />);
    }
  }
}

function mapStateToProps({UtilisateurSession}){
  return {UtilisateurSession};
}

export default connect(mapStateToProps)(SessionRoute);
