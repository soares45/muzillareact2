import React, {Component} from 'react';
import Loader from './header/loader';
import SearchBar from './header/SearchBar';
import TopBar from './header/topBar';
import SideBar from './header/sideBar';

class Header extends Component {

  render() {
    return (
      <div>
        {/* Page Loader */}
        <Loader />
        {/* Overlay For Sidebars */}
        <div className="overlay" />
        {/* Search Bar */}
        <SearchBar />
        {/* Top Bar */}
        <TopBar />
        {/* Sidebar */}
        <SideBar />
      </div>
    );
  }
}

export default Header;
