import React, {Component} from 'react';

class InscriptionLogo extends Component {

  render() {
    return (
      <div className="logo">
        <a href="javascript:void(0);"><b>Muzilla</b></a>
        <small>Service de streaming musical - Muzilla</small>
      </div>
    );
  }
}

export default InscriptionLogo;
