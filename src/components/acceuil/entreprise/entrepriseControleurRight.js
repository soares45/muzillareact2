import React, {Component} from 'react';

class EntrepriseControleurLeft extends Component {

  render() {
    return (
      <a className="right carousel-control" href="#carousel-example-generic_3" role="button" data-slide="next">
        <span className="glyphicon glyphicon-chevron-right" aria-hidden="true" />
        <span className="sr-only">Next</span>
      </a>
    );
  }
}

export default EntrepriseControleurLeft;
