import React, {Component} from 'react';
import MusiqueList from './musiqueList';
import MusiqueListItem from './musiqueListItem';

class PlaylistItem extends Component {

  render() {
    return (
      <div>
        <a href="javascript:void(0);" className="menu-toggle">
          <i className="material-icons">library_music</i>
          <span>Playlist #1</span>
        </a>
        <ul className="ml-menu">
          <li>
            <a href="pages/Playlist.html">
              <span>Voir Playlist #1</span>
            </a>
          </li>
          {/* <MusiqueList /> */}
          <MusiqueListItem />
          <MusiqueListItem />
          <MusiqueListItem />
          <MusiqueListItem />
          <MusiqueListItem />
        </ul>
      </div>
    );
  }
}

export default PlaylistItem;
