import React, {Component} from 'react';

class InscriptionPassword extends Component {

  render() {
    return (
      <div className="input-group">
        <span className="input-group-addon">
          <i className="material-icons">lock</i>
        </span>
        <div className="form-line">
          <input type="password" className="form-control" name="password" minLength={6} placeholder="Mot de passe" required />
        </div>
      </div>
    );
  }
}

export default InscriptionPassword;
