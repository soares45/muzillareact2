import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch} from 'react-router-dom';

import promise from 'redux-promise';
import reducers from './reducers';
import Acceuil from './components/acceuil';
import Login from './components/login';
import Inscription from './components/inscription';
import Presentation from './components/presentation';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <Switch>
          <Route path="/accueil" component={Acceuil} />
          <Route path="/inscription" component={Inscription} />
          <Route path="/connection" component={Login} />
          <Route path="/" component={Presentation} />
      </Switch>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
