import React, {Component} from 'react';
import Playlist from './playlist';
import PlaylistItem from './playlistItem';

class Bibiotheque extends Component {

  render() {
    return (
      <div>
        <li className="header">BIBIOTHÈQUE</li>
        {/* <Playlist /> */}
        <PlaylistItem />
        <PlaylistItem />
        <PlaylistItem />
        <PlaylistItem />
        <PlaylistItem />
      </div>
    );
  }
}

export default Bibiotheque;
