import React, {Component} from 'react';
import EntrepriseHeader from './entreprise/entrepriseHeader';
import EntrepriseBody from './entreprise/entrepriseBody';

class EntrepriseListItem extends Component {

  render() {
    return (
      <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div className="card">
          <EntrepriseHeader />
          <EntrepriseBody />
        </div>
      </div>
    );
  }
}

export default EntrepriseListItem;
