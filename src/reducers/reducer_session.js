import _ from 'lodash';
import { FETCH_LOGIN } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case FETCH_LOGIN:
      return action.payload.data;
      break;
    default:
      return state;
  }
}
