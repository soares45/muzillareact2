
import React, {Component} from 'react';

class TopBar extends Component {

  render() {
    return (
      <nav className="navbar" style={{backgroundColor: '#F44336'}}>
        <div className="container-fluid">
          <div className="navbar-header">
            <a href="javascript:void(0);" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" />
            <a href="javascript:void(0);" className="bars" style={{color: '#fff'}}/>
            <a className="navbar-brand" href="index.html" style={{marginLeft: '28px', color: '#fff'}}>Muzilla - Service de streaming musical</a>
          </div>
          <div className="collapse navbar-collapse" id="navbar-collapse">
            <ul className="nav navbar-nav navbar-right">
              {/* Call Search */}
              <li><a href="javascript:void(0);" className="js-search" data-close="true" style={{color: '#fff'}}><i className="material-icons">search</i></a></li>
              {/* #END# Call Search */}
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default TopBar;
