import _ from 'lodash';
import { AJOUT_MUSIQUE_LECTEUR } from '../actions';

export default function(state = {}, action){
  switch (action.type) {
    case AJOUT_MUSIQUE_LECTEUR:
      return [action.payload.musique, ...state];
      break;
    default:
      return state;
  }
}
