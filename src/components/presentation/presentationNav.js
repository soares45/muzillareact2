import React, {Component} from 'react';

class PresentationNav extends Component {

  render() {
    return (
          <div className="row" style={{width: '100%', position: 'absolute', top: 0, margin: 0, zIndex: 1001, left: '0px'}}>
            <div className="col-xs-3" style={{fontSize: 20, textTransform: 'uppercase', fontWeight: 'bold'}}>
              <div id="gtco-logo" style={{fontSize: 20, margin: 0, padding: 0, textTransform: 'uppercase', fontWeight: 'bold'}}>
                <a href="index.html" style={{color: 'white'}}>Muzilla.</a>
              </div>
            </div>
            <div className="col-xs-2 "></div>
            <div className="col-xs-2" style={{fontSize: 20, textTransform: 'uppercase', fontWeight: 'bold'}}>
              <div className="center" id="gtco-logo" style={{fontSize: 20, margin: 0, padding: 0, textTransform: 'uppercase', fontWeight: 'bold', textAlign: 'center'}}>
                <a href="index.html" style={{color: 'white'}}>S`incrire.</a>
              </div>
            </div>
            <div className="col-xs-2 "></div>
            <div className="col-xs-3" style={{fontSize: 20, textTransform: 'uppercase', fontWeight: 'bold'}}>
              <div className="right" id="gtco-logo" style={{fontSize: 20, margin: 0, padding: 0, textTransform: 'uppercase', fontWeight: 'bold'}}>
                <a href="index.html" style={{color: 'white'}}>Se connecter.</a>
              </div>
            </div>
          </div>
    );
  }
}

export default PresentationNav;
