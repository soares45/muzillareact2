import React, {Component} from 'react';
import CarouselList from './carousel/carouselList';
import CarouselListItem from './carousel/carouselListItem';
import EntrepriseControleurLeft from './entrepriseControleurLeft';
import EntrepriseControleurRight from './entrepriseControleurRight';

class EntrepriseBody extends Component {

  render() {
    return (
      <div className="body">
        <div id="carousel-example-generic_3" className="carousel slide" data-ride="carousel">
          {/* Indicators */}
          <ol className="carousel-indicators">
            <li data-target="#carousel-example-generic_3" data-slide-to={0} className="active" />
            <li data-target="#carousel-example-generic_3" data-slide-to={1} />
            <li data-target="#carousel-example-generic_3" data-slide-to={2} />
          </ol>
          {/* Wrapper for slides */}
          {/* <CarouselList />*/}
          {/*<div className="carousel-inner" role="listbox">*/}
            {/*<CarouselListItem />*/}
            {/*<CarouselListItem />*/}
            {/*<CarouselListItem />*/}
          {/*</div>*/}
          <div className="carousel-inner" role="listbox">
            <div className="item active">
              <img src="images/image-gallery/10.jpg" />
              <div className="carousel-caption">
                <h3>Album #1</h3>
                <p>Je suis une prensentation de lalbum.</p>
              </div>
            </div>
            <div className="item">
              <img src="images/image-gallery/12.jpg" />
              <div className="carousel-caption">
                <h3>Album #2</h3>
                <p>Je suis une prensentation de lalbum.</p>
              </div>
            </div>
            <div className="item">
              <img src="images/image-gallery/19.jpg" />
              <div className="carousel-caption">
                <h3>Album #3</h3>
                <p>Je suis une prensentation de lalbum.</p>
              </div>
            </div>
          </div>
          {/* Controls */}
          <EntrepriseControleurLeft />
          <EntrepriseControleurRight />
        </div>
      </div>
    );
  }
}

export default EntrepriseBody;
