import React from 'react';
import MusiqueListItem from './musiqueListItem';

const EntrepriseList = ({videos}) => {
  const MusiqueListItems = videos.map((video) => {
    return (
      <MusiqueListItem />
    );
  });
  return (
    <div className="row">
      {MusiqueListItems}
    </div>
  );
};

export default EntrepriseList;
