import React from 'react';
import CarouselListItem from './carouselListItem';

const CarouselList = ({videos, onVideoSelect}) => {
  const CarouselListItems = videos.map((video) => {
    return (
      <CarouselListItem
        onVideoSelect={onVideoSelect}
        key={video.etag}
        video={video} />
    );
  });
  return (
    <div className="carousel-inner" role="listbox">
      {CarouselListItems}
    </div>
  );
};

export default CarouselList;
